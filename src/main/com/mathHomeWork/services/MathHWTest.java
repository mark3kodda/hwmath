package main.com.mathHomeWork.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import test.com.mathHomeWork.services.MathHW;

import java.text.DecimalFormat;

import static org.junit.jupiter.api.Assertions.*;

class MathHWTest {

    MathHW cut = new MathHW();
    private static final DecimalFormat dfZero = new DecimalFormat("0.00");


    static Arguments[] shootingCannonInDegreeTestArgs(){
        return new Arguments[]{
                Arguments.arguments(50,17,"142.65"),
                Arguments.arguments(0,25,"0.00"),
                Arguments.arguments(100,90,"0.00"),
                Arguments.arguments(100,-90,"-0.00"),
                Arguments.arguments(85,65,"564.76")
        };
    }

    @ParameterizedTest
    @MethodSource("shootingCannonInDegreeTestArgs")
    void shootingCannonInDegreeTest(double speed, double initialSpeed, String expected ) {
        String actual = cut.shootingCannonInDegree(speed,initialSpeed);
        Assertions.assertEquals(expected,actual);

    }

    static Arguments[] shootingCannonInRadTestArgs(){
        return new Arguments[]{
                Arguments.arguments(50,17,"142.65"),
                Arguments.arguments(0,25,"0.00"),
                Arguments.arguments(100,90,"0.00"),
                Arguments.arguments(100,-90,"-0.00"),
                Arguments.arguments(85,65,"564.76")
        };
    }

    @ParameterizedTest
    @MethodSource("shootingCannonInRadTestArgs")
    void shootingCannonInRadTest(double speed, double initialSpeed, String expected ) {
        String actual = cut.shootingCannonInDegree(speed,initialSpeed);
        Assertions.assertEquals(expected,actual);

    }

    static Arguments[] getDistanceBetweenCarsTestArgs(){
        return new Arguments[]{
                Arguments.arguments(50,25,10,2,160),
                Arguments.arguments(10,15,0,3,75),
                Arguments.arguments(50,0,0,4,200),
                Arguments.arguments(0,0,10,2,10),
                Arguments.arguments(50,-25,0,2,50)
        };
    }

    @ParameterizedTest
    @MethodSource("getDistanceBetweenCarsTestArgs")
    void getDistanceBetweenCarsTest(int speed1, int speed2, int distanceBetween, int time, int expected) {
        int actual = cut.getDistanceBetweenCars(speed1,speed2,distanceBetween,time);
        Assertions.assertEquals(expected,actual);
    }

    static Arguments[] checkIfDotInAreaTestArgs(){
        return new Arguments[]{
                Arguments.arguments(1.5, 1.7, 0),
                Arguments.arguments(5, 2.2, 0),
                Arguments.arguments(0.5, 0.2, 1),
                Arguments.arguments(0.5, 1.5, 0)
        };
    }

    @ParameterizedTest
    @MethodSource("checkIfDotInAreaTestArgs")
    void checkIfDotInAreaTest(double x, double y, int expected) {
        double actual = cut.checkIfDotInArea( x, y);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] computeFormulaTestArgs(){
        return new Arguments[]{
                Arguments.arguments(12.5,4.97),
                Arguments.arguments(34.12,Double.NaN)
        };
    }

    @ParameterizedTest
    @MethodSource("computeFormulaTestArgs")
    void computeFormulaTest(double condition, double expected) {
        double actual = cut.computeFormula(condition);
        Assertions.assertEquals(dfZero.format(expected),dfZero.format(actual));

    }
}