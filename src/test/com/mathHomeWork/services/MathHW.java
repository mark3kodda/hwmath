package test.com.mathHomeWork.services;

import java.text.DecimalFormat;
public class MathHW {

    private static final double GRAVITY = 9.8;
    private static final DecimalFormat dfZero = new DecimalFormat("0.00");

    public String shootingCannonInDegree(double speed,double initialAngle){
        double result = ((speed * speed) / GRAVITY) * Math.sin(Math.toRadians(2 * initialAngle));

        return dfZero.format(result);
    }

    public String shootingCannonInRad(double speed, double initialAngle){
        double result = ((speed * speed) * Math.sin(2 * initialAngle)) / GRAVITY;
        return dfZero.format(result);
    }

    public int getDistanceBetweenCars(int speed1,int speed2, int distanceBetweenCars,int time){

        return (speed1+speed2)*time+distanceBetweenCars;
    }

    public int checkIfDotInArea(double x, double y){

        if (((x >= 0) && (y >=1.5 * x -1) && (y <= x)) || ((x <= 0) && (y >= -1.5 * x -1) && (y <= -x))){
            return 1;
        } else {
            return 0;
        }
    }

    public double computeFormula(double x) {
        double a = Math.exp(x + 1) + 2d * Math.exp(x) * Math.cos(x);
        double b = x - Math.exp(x + 1) * Math.sin(x);
        double result = (6d * Math.log1p(Math.sqrt(a))) / (Math.log1p(b)) + Math.abs(Math.cos(x) / Math.exp(Math.sin(x)));
        return result;

    }



}
